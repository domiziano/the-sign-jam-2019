﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Container
{
    public AnimatorOverrideController newController;
    public GameObject weaponToActivate;
}

public class ChangeHumanWeaponBehaviour : MonoBehaviour
{

    public Animator animator;
    public List<Container> containerList = new List<Container>();
    private GameObject currentWeapon;

    // Start is called before the first frame update
    void Start()
    {
        ChangeToNewOverride(containerList[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if(CheckStuff(KeyCode.Alpha0))
        {
            ChangeToNewOverride(containerList[0]);
        }
        else if(CheckStuff(KeyCode.Alpha1))
        {
            ChangeToNewOverride(containerList[1]);
        }
        else if (CheckStuff(KeyCode.Alpha2))
        {
            ChangeToNewOverride(containerList[2]);
        }

        if(CheckStuff(KeyCode.Space))
        {
            animator.Play("Attack");
        }

    }

    void ChangeToNewOverride(Container container)
    {
        /*AnimatorStateInfo[] layerInfo = new AnimatorStateInfo[animator.layerCount];
        for(int i = 0; i <animator.layerCount; i++)
        {
            layerInfo[i] = animator.GetCurrentAnimatorStateInfo(i);
        }
        animator.runtimeAnimatorController = container.newController;
        for(int i = 0; i < animator.layerCount; i++)
        {
            animator.Play(layerInfo[i].fullPathHash, i, layerInfo[i].normalizedTime);
        }
        animator.Update(0.0f);
        */
        animator.runtimeAnimatorController = container.newController;
        animator.Play("Equip Weapon");
        container.weaponToActivate.SetActive(true);
        if (currentWeapon != null && currentWeapon != container.weaponToActivate)
        {
            currentWeapon?.SetActive(false);
        }
        currentWeapon = container.weaponToActivate;
    }

    public bool CheckStuff(KeyCode n)
    {
        if(Input.GetKeyDown(n))
        {
            return true;
        }
        return false;
    }
}
