﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Playerstats", fileName = "Playerstats")]
public class PlayerStats : ScriptableObject
{
    public float forwardSpeed, backwardSpeed;
    public float maxHealth;
}
