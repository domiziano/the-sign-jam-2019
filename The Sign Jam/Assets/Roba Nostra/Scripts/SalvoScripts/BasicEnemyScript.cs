﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicEnemyScript : MonoBehaviour, IDamageable
{
    public static int killCount = 0;
    [SerializeField]private WeaponManager weaponManager;
    [SerializeField]private float currentHp;
    [SerializeField]private PlayerStats enemyStats;
    [SerializeField]private bool isDead = false;
    private GameObject ragdoll;
    private GameObject renderObject;
    private Vector3 rbVelocity;

    public Vector3 RbVelocity => rbVelocity;
    void Start()
    {
        weaponManager = GetComponent<WeaponManager>();
        ragdoll = transform.GetChild(0).gameObject;
        ragdoll.SetActive(false);
        renderObject = transform.GetChild(1).gameObject;
        currentHp = enemyStats.maxHealth;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T)) TakeDamage(11000);
        weaponManager.DecreaseWeaponCooldown();
    }

    public void EnemyNearby(Animator anim)
    {
        if (weaponManager.CanCurrentWeaponAttack())
        {
            weaponManager.UseWeapon(anim);
        }
    }

    public void TakeDamage(float damage)
    {
        if(isDead) return;
        currentHp -= damage;
        if(currentHp <= 0)
        {
            isDead = true;
            Die();
        }
    }

    // when player dies : deactivate navmesh, box collider on locomotion, animator, ragdoll ON
    public void Die()
    {
        killCount++;
        //actual velocity
        rbVelocity = GetComponent<Rigidbody>().velocity;
        ragdoll.GetComponent<RagdollBehaviour>().GetVelocity(rbVelocity);
        ragdoll.transform.SetParent(transform.root);
        ragdoll.SetActive(true);
        ragdoll.GetComponent<RagdollBehaviour>().ApplyRigidbodiesVelocity();
        //gameObject.SetActive(false);
        Debug.Log("Me ded");
        
        renderObject.SetActive(false);
        Invoke("DeactivateThisGameobject", 0);
    }

    private void DeactivateThisGameobject()
    {
        gameObject.SetActive(false);
    }
    public void OnHit()
    {

    }
}
