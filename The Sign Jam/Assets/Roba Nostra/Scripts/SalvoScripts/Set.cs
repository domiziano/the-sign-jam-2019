﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Set", menuName = "Set")]
public class Set : ScriptableObject
{
    public List<EnemyPosition> enemiesPos = new List<EnemyPosition>();
}

[System.Serializable]
public class EnemyPosition
{
    public GameObject enemy;
    public Transform enemyTransform;
}