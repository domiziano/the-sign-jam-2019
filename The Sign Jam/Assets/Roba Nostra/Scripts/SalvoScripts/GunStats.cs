﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponStats", menuName = "WeaponStats")]
public class GunStats : ScriptableObject
{
    public float damage, range, projectileSpeed;
}
