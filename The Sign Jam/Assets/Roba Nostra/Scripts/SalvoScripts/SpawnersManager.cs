﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpawnersManager : MonoBehaviour
{
    public static int enemiesSpawned;
    public List<SpawnerManagerSettings> managerSettingsStars = new List<SpawnerManagerSettings>();
    public List<Spawner> spawners = new List<Spawner>();
    private float currentSpawnTime;
    

    // Start is called before the first frame update
    void Start()
    {
        currentSpawnTime = managerSettingsStars[0].timeToSpawnNext;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.hasPlayerShot)
        {
            currentSpawnTime -= Time.deltaTime;
            if(currentSpawnTime <= 0)
            {
                SpawnStuff();
            }
        }
    }

    public void SpawnStuff()
    {
        currentSpawnTime = managerSettingsStars[GameManager.stars].timeToSpawnNext;
        var playerPos = GameManager.ReturnPlayerPosition();
        List<UsefulClass> notSoUseful = new List<UsefulClass>();
        for(int i = 0; i < spawners.Count; i++)
        {
            var x = new UsefulClass();
            x.spawner = spawners[i];
            x.distance = Vector3.Distance(spawners[i].transform.position, playerPos);
            notSoUseful.Add(x);
        }
        notSoUseful = notSoUseful.OrderBy(a => a.distance).ToList();
        for(int i = 0; i < managerSettingsStars[GameManager.stars].numberOfSpawners; i++)
        {
            notSoUseful[i].spawner.Spawn();
        }
        managerSettingsStars[GameManager.stars].howManyWavesLeft -= 1;
        if(managerSettingsStars[GameManager.stars].howManyWavesLeft <= 0)
        {
            GameManager.stars++;
        }
    }
}

public class UsefulClass
{
    public Spawner spawner;
    public float distance;
}


[System.Serializable]
public class SpawnerManagerSettings
{
    public float timeToSpawnNext, howManyWavesLeft, numberOfSpawners;
}
