﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using Cinemachine;
public class PlayerController : MonoBehaviour, IDamageable
{
    public enum PlayerState { alive, dead, invincible };
    public PlayerState actualState = PlayerState.alive;

    public KeyCode attackInput;
    public WeaponManager weaponManager;
    public Animator anim;
    public SkinnedMeshRenderer meshRenderer;

    [SerializeField]private PlayerStats stats;
    [SerializeField]private Rigidbody rb;
    [SerializeField]private float currentHealth, onHitInvincibilityDuration, invincibilityTimeTick;
    private Player player;
    private CinemachineVirtualCamera virtualCamera;
    private Transform worldZero;
    // Start is called before the first frame update
    void Start()
    {
        player = ReInput.players.GetPlayer(0); 
        rb = GetComponent<Rigidbody>();
        weaponManager.SwitchWeapon(anim, 0);
        currentHealth = stats.maxHealth;
        worldZero = GameObject.Find("Vector3 zero").transform;
        virtualCamera = GameObject.Find("CM vcam1").GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetButton("Fire"))
        {
            if (weaponManager.CanCurrentWeaponAttack())
            {
                weaponManager.UseWeapon(anim);
            }
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            weaponManager.SwitchWeapon(anim, -1);
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            weaponManager.SwitchWeapon(anim, 1);
        }
        weaponManager.DecreaseWeaponCooldown();
        if(rb.velocity.magnitude > 0.1f)
        {
            anim.SetFloat("MovementSpeed", 1);
        }
        else
        {
            anim.SetFloat("MovementSpeed", 0);
        }
    }

    public void Die()
    {
        //GameManager.PlayerDeath();
        Destroy(gameObject);
    }

    public void TakeDamage(float damage)
    {
        if (actualState == PlayerState.alive)
        {
            player.SetVibration(0,2,0.3f);
            currentHealth -= damage;
            if (currentHealth <= 0)
            {

                Die();

                virtualCamera.m_Follow = worldZero;
                virtualCamera.m_LookAt = worldZero;
            }
            else
            {
                StartCoroutine(FadeOutIn());
            }
        }
    }

    public void OnHit()
    {

    }

    IEnumerator FadeOutIn()
    {
        actualState = PlayerState.invincible;
        var startingMat = meshRenderer.material;
        var dur = 0f;
        var counter = 0;
        while(dur < onHitInvincibilityDuration)
        {
            var newColor = startingMat.color;
            if (counter % 2 == 0)
            {
                newColor.a = 0.5f;
                meshRenderer.material.color = newColor;
            }
            else
            {
                newColor.a = 1f;
                meshRenderer.material.color = newColor;
            }
            Debug.Log(newColor);
            counter++;
            dur += invincibilityTimeTick;
            yield return new WaitForSeconds(invincibilityTimeTick);
        }
        var oldColor = startingMat.color;
        oldColor.a = 1f;
        meshRenderer.material.color = oldColor;
        actualState = PlayerState.alive;
    }

}
