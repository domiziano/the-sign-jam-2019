﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public List<Star> stars = new List<Star>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn()
    {
        Debug.Log(name);
        var x = Random.Range(0, stars[GameManager.stars].sets.Count);
        for (int i = 0; i < stars[GameManager.stars].sets[x].enemiesPos.Count; i++)
        {
            var enemyInfo = stars[GameManager.stars].sets[x].enemiesPos[i];
            Instantiate(enemyInfo.enemy, enemyInfo.enemyTransform.position, Quaternion.identity);
            Debug.Log(enemyInfo.enemyTransform.position);
        }
    }

}

[System.Serializable]
public class Star
{
    public List<Set> sets = new List<Set>(); 
}

