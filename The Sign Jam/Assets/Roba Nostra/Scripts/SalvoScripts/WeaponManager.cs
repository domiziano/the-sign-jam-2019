﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public List<Container> containerList = new List<Container>();
    private int currentWeapon = 0;
    [SerializeField]private GameObject currentWeaponObject;

    public void SwitchWeapon(Animator anim, int direction)
    {
        currentWeapon += direction;
        if(currentWeapon >= containerList.Count)
        {
            currentWeapon = 0;
        }
        else if(currentWeapon < 0)
        {
            currentWeapon = containerList.Count - 1;
        }

        anim.runtimeAnimatorController = containerList[currentWeapon].newController;
        anim.Play("Equip Weapon");
        containerList[currentWeapon].weaponToActivate.SetActive(true);
        if (currentWeaponObject != null && currentWeaponObject != containerList[currentWeapon].weaponToActivate)
        {
            currentWeaponObject?.SetActive(false);
        }
        currentWeaponObject = containerList[currentWeapon].weaponToActivate;
    }

    public bool CanCurrentWeaponAttack()
    {
        return currentWeaponObject.GetComponent<Weapon>().CanAttack();
    }

    public void DecreaseWeaponCooldown()
    {
        foreach(var container in containerList)
        {
            container.weaponToActivate.GetComponent<Weapon>().actualCooldown -= Time.deltaTime;
        }
    }

    public void UseWeapon(Animator anim)
    {
        currentWeaponObject.GetComponent<Weapon>()?.Attack(anim);
    }

}
