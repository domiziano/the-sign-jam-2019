﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IDamageable
{
    void Die();
    void TakeDamage(float damage);
    void OnHit();
}
