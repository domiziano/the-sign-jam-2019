﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    [SerializeField] private Collider coll;
    [SerializeField] private float damage, colliderDuration;
    private List<GameObject> enemiesHit = new List<GameObject>();

    private void Start()
    {
        coll.enabled = false;
    }

    public override void Attack(Animator anim)
    {
        anim.Play("Attack");
        StartCoroutine(ColliderManaging());
        enemiesHit.Clear();
        actualCooldown = weaponCooldown;
    }

    private void OnTriggerEnter(Collider other)
    { 
        if(other.GetComponent<IDamageable>() != null && !enemiesHit.Contains(other.gameObject))
        {
            other.GetComponent<IDamageable>().TakeDamage(damage);
            enemiesHit.Add(other.gameObject);
        }
    }

    IEnumerator ColliderManaging()
    {
        coll.enabled = true;
        yield return new WaitForSeconds(colliderDuration);
        coll.enabled = false;
    }
}
