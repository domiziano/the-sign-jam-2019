﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    [SerializeField]private GameObject particleOnHit;
    [SerializeField]private float explosiveForce;
    [HideInInspector]public float range, damage;
    [HideInInspector]public Vector3 startingPoint;

    public void Start()
    {
        startingPoint = transform.position;
    }

    public void Update()
    {
        if(Vector3.Distance(transform.position,startingPoint) > range)
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<IDamageable>() != null)
        {
            other.GetComponent<IDamageable>().TakeDamage(damage);
            //other.GetComponent<Rigidbody>().AddForce(other.gameObject.transform.forward * -1 * explosiveForce, ForceMode.VelocityChange);
        }
        Instantiate(particleOnHit, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void Explode()
    {

    }
}
