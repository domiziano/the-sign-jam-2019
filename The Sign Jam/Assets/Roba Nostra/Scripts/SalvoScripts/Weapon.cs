﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]protected float weaponCooldown;
    [HideInInspector]public float actualCooldown;

    public virtual void Attack(Animator anim)
    {

    }

    public bool CanAttack()
    {
        if(actualCooldown  <= 0)
        {
            return true;
        }
        return false;
    }
}
