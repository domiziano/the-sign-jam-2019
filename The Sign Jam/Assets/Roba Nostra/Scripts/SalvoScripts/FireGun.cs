﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGun : Weapon
{
    public GunStats stats;
    public GameObject projectile;
    public GameObject barrel;
    public GameObject outDirectionBarrel;
    public GameObject fireParticle;

    public override void Attack(Animator anim)
    {
        GameObject go = Instantiate(projectile, barrel.transform.position, Quaternion.identity);
        //Instantiate(fireParticle, barrel.transform.position, Quaternion.identity);
        var projectileScript = go.GetComponent<BaseProjectile>();
        projectileScript.range = stats.range;
        projectileScript.damage = stats.damage;
        var rb = go.GetComponent<Rigidbody>();
        rb.AddForce((outDirectionBarrel.transform.position - barrel.transform.position).normalized * stats.projectileSpeed, ForceMode.Impulse);

        actualCooldown = weaponCooldown;
    }
}
