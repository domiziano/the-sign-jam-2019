﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int stars;
    public static bool hasPlayerShot = true;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        stars = 0;
        BasicEnemyScript.killCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }
    }

    public static Vector3 ReturnPlayerPosition()
    {
        var x = GameObject.FindGameObjectWithTag("Player");
        return (x != null) ? x.transform.position : Vector3.zero ;
    }

    private void Restart()
    {
        SceneManager.LoadScene("Main");
    }
}
