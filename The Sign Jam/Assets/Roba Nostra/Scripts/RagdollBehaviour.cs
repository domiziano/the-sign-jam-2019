﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollBehaviour : MonoBehaviour
{
    public float forceAmount;
    private GameObject root;
    private Rigidbody[] rigidbodies;

    private Vector3 velocityEnemyWhenDied;

    // Start is called before the first frame update
    void Awake()
    {
        root = transform.Find("EnemyMesh/Root").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetVelocity(Vector3 velocity)
    {
        velocityEnemyWhenDied = velocity;
    }
    public void ApplyRigidbodiesVelocity()
    {
        rigidbodies = root.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rigidbodies)
        {
            //rb.velocity = velocityEnemyWhenDied;
            rb.AddExplosionForce(forceAmount,rb.position,50f);
        }
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.mass = 1;
        }
    }
}
