﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Rewired;
using UnityEngine;
using Plane = UnityEngine.Plane;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;
using Cinemachine;

public class PlayerLocomotion : MonoBehaviour
{
    public FloatVariable speed;
    public FloatVariable speedRotation;
    public bool usingMouse = true;
    public AnimationCurve curve;
    private Rigidbody rb;
    private Camera cam;
    private Player player;
    public LayerMask enemyAndPlaneLayer;
    private CinemachineVirtualCamera virtualCam;
    private GameObject aimArm;
    
    private Vector3 movementDirection = Vector3.zero;
    private float x;
    private float z;
    private Vector3 backupRotationDirection = Vector3.right;
    private float t = 0;
    private bool fire = false;
    public bool Fire => fire;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        aimArm = GameObject.Find("AimArm");
        rb = GetComponent<Rigidbody>();
        player = ReInput.players.GetPlayer(0);
        virtualCam = GameObject.Find("CM vcam1").GetComponent<CinemachineVirtualCamera>();
        
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        if (usingMouse)
        {
            LookWithMouse(GetDirection());
        }
        else
        {
            LookWithController(GetDirection());
        }
    }

    private void FixedUpdate()
    {
        Move(movementDirection);
    }

    private void GetInput()
    {
        x = player.GetAxis("Move Horizontal");
        z = player.GetAxis("Move Forward");
        fire = player.GetButton("Fire");
    }
    private void Move(Vector3 direction)
    {
        // get direction vector with input values
        direction = new Vector3(x,0,z);
        // get can proper angle 
        float cameraFacing = cam.transform.eulerAngles.y;
        Vector3 fixedDirection = Quaternion.Euler(0, cameraFacing, 0) * direction;
        float duration = 3f;

        t += Time.deltaTime / duration;
        //t = Time.time%1;
        rb.velocity = (speed.Variable * fixedDirection.normalized * curve.Evaluate(t));
    }

    private Quaternion LookWithMouse(Vector3 directionToLook)
    {
        /*
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = cam.ScreenPointToRay (directionToLook);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        Quaternion targetRotation = new Quaternion(0,0,0,0);
        if (playerPlane.Raycast (ray, out hitdist)) 
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speedRotation.Variable * Time.deltaTime);
        }
        // transform.LookAt(new Vector3(direction.x, transform.position.y, direction.z));
        // Debug.DrawLine(Vector3.zero,new Vector3(direction.x, transform.position.y, direction.z),Color.red);
        return targetRotation;
        */
        var mousePos = Input.mousePosition;
        var ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;
        if(Physics.Raycast(ray,out hit, 1000f, enemyAndPlaneLayer))
        {
            transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
        }
        return Quaternion.identity;
    }

    public void MyLookAt(Vector3 whereToLook)
    {

    }

    private void LookWithController(Vector3 finalDirection)
    {
        float cameraFacing = cam.transform.eulerAngles.y;
        finalDirection.Normalize();
        finalDirection = Quaternion.Euler(0, cameraFacing, 0) * finalDirection;
        aimArm.transform.position =  (transform.position + finalDirection);
        transform.rotation = Quaternion.LookRotation(finalDirection);
        
    }
    private Vector3 GetDirection()
    {
        if (usingMouse)
        {
            return Input.mousePosition;
        }
        else
        {
            Vector3 rotationDirection = new Vector3(player.GetAxis("Aim Horizontal"), 0, player.GetAxis("Aim Forward"));
            
            // if no input on rotation
            if (rotationDirection.x == 0 && rotationDirection.z == 0)
            {
                return backupRotationDirection;
            }
            else
            {
                backupRotationDirection = rotationDirection;
                return  rotationDirection;    
            }
            
        }
    }
}
