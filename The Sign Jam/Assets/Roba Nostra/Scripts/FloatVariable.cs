using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
	[SerializeField] private float variable = 1;
	public float Variable => variable;
}