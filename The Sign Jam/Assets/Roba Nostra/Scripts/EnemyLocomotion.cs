﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.AI;

public class EnemyLocomotion : MonoBehaviour
{
    public enum State { following, attacking, unableToMove}
    public State currentState;

    public float startAttackDistance = 10f;
    [SerializeField]private BasicEnemyScript basic;
    [SerializeField]private Animator anim;
    private Vector3 playerPos;
    private NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        currentState = currentState == State.unableToMove ? State.unableToMove : State.following;
        basic = GetComponent<BasicEnemyScript>();
        agent = transform.root.GetComponentInChildren<NavMeshAgent>();
        agent.avoidancePriority = UnityEngine.Random.Range(0, 101);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentState == State.unableToMove)
        {
            return;
        }
        playerPos = GameManager.ReturnPlayerPosition();
        var distanceFromPlayer = Vector3.Distance(playerPos, transform.position);
        if (currentState == State.following)
        {
            agent.SetDestination(playerPos);
            if (distanceFromPlayer <= startAttackDistance)
            {
                currentState = State.attacking;
                agent.isStopped = true;
            }
        }
        if(currentState == State.attacking)
        {
            transform.LookAt(playerPos);
            basic.EnemyNearby(anim);
            if(distanceFromPlayer > startAttackDistance)
            {
                agent.isStopped = false;
                currentState = State.following;
            }
        }
        
    }

    void FixedUpdate()
    {

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(playerPos, 0.5f);
    }
}
